#!/bin/bash
# Count number of lines in the  file
# echo "Vocabulary count"
# wc -l ./mlvocabfreq.tsv
# echo "Cumulative Frequency count"
# awk -F'\t' '{s+=$2}END{print s}' ./mlvocabfreq.tsv

# Create a copy of vocabulary frequency file for processing
# cat ./mlvocabfreq.tsv > ./mlcopy.tsv
filename='./mlcopy.tsv'
# echo "Removing special characters"
# sed -i -e "/'/d" $filename
# sed -i -e '/"/d' $filename
# sed -i -e '/\[/d' $filename
# sed -i -e '/\]/d' $filename
# sed -i -e '/\$/d' $filename
# sed -i -e '/\*/d' $filename
# sed -i -e '/\&/d' $filename
# sed -i -e '/[<{`!%}_:#^?>;|=~\\,()+\/@-]/d' $filename
# sed -i -e '/\./d' $filename
# sed -i -e '/ംഃ/d' $filename
# sed -i -e '/്ഃ/d' $filename
# sed -i -e 's/ൌ/ൗ/g' $filename
# echo "Cleaned up vocabulary count"
# wc -l $filename
# echo "Cleaned up Cumulative Frequency count"
# awk -F'\t' '{s+=$2}END{print s}' $filename

# Most frequent words
# head -1000 $filename > mostfreq.tsv
# mostfreqfile='./mostfreq.tsv'
# echo "Cumulative Frequency count of most frequent 1000 words"
# awk -F'\t' '{s+=$2}END{print s}' $mostfreqfile
# cut -f 1 $mostfreqfile > freqwords.txt

# # Common 100k words. Extar 700 words are taken, because erroneous contents will be deleted in the folowing step.
head -100700 $filename > toplakh.tsv
toplakh='./toplakh.tsv'
wc -l $toplakh
# echo "Cumulative Frequency count of common 100k words"
# awk -F'\t' '{s+=$2}END{print s}' $toplakh

#################
# Create a lexicon from the words in toplakh.tsv and save in lexicons/egs.tsv
################

# Delete words which could not be analysed by mlphon to create a lexicon. 
LINES=$(ack "\?" lexicons/egs.tsv | cut -f 1 ) #lexicons/egs.tsv is the lexicon created from words in toplakh.tsv
for LINE in $LINES
do
    echo $LINE
    read -p "Do you want to delete this? (y/n)" response
    if [ "$response" = "y" ] 
    then
        sed -i "/^$LINE\t/d" $toplakh
    fi
    wc -l $toplakh
done

# Interactively delete words with ഃ, which is mostly a typo
LINES=$(ack "ഃ" toplakh.tsv | cut -f 1)
for LINE in $LINES
do
    echo $LINE
    read -p "Do you want to delete this? (Press y for deletion. Press any key to continue.)" response
    if [ "$response" = "y" ] 
    then
        sed -i "/^$LINE\t/d" $toplakh
    fi
    wc -l $toplakh
done

cut -f 1 $toplakh | head -100000 > commonwords.txt