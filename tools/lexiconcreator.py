import argparse
from mlphon import PhoneticAnalyser, phonemize

from sys import stderr, stdin, stdout
from datetime import datetime

def createlexicon(analyser, infile, outfile):
  lines = infile.readlines()
  for line in lines:
      line = line.strip()
      if not line or line == '':
          continue
      middle_vowel_index = [idx for idx, ele in enumerate(line) if ele in 'എഇഐഒഓആഅ'] #Check is there are any vowels in word -medial positions that can possibly be from abbreviations
      if len(middle_vowel_index):
        if middle_vowel_index[0] >0:
          middle_vowel_index.insert(0,0)
        parts = [line[i:j] for i,j in zip(middle_vowel_index, middle_vowel_index[1:]+[None])]
      else:
        parts =  [line]
      results = []
      outfile.write(line+"\t")
      transcript = ''
      for element in parts: #eg: parts = [എൽഡി, എഫ്], parts = [എന്നാൽ]
        try:
          analysis = analyser.analyse(element)
        except ValueError as error_instance:
          transcript=transcript+("?")
        else:
          results.append(analysis)
      for result in results:
        for i in range(len(result)):
          analysis= result[i]
          if i>=1:
            outfile.write(transcript+"\n"+line+"\t")
            transcript=''
          transcript=transcript+(str(phonemize(analysis," ","")))
      outfile.write(transcript)
      outfile.write("\n")

infile = open("./propernouns.lex", "r")
outfile = open("./lexicons/propernounslexicon.tsv", "a")
analyser = PhoneticAnalyser()
startTime = datetime.now()
createlexicon(analyser, infile, outfile)
print("Execution Time", datetime.now() - startTime)
