This project maintains a gold standard phonetic lexicon of 1000 frequently used words in Malayalam. A large vocabulary phonetic lexicon created using mlphon toolkit is also maintained here.

The source of Malayalam word vocabulary frequency: https://github.com/AI4Bharat/indicnlp_corpus

- The source `mlvocabfreq.tsv` has  8830462 unique word (Contains inflected and agglutinated words)

- After cleaning it up for words containing punctuations, there are 8829790 unique words

- The most common 100k words from the above list is extracted and stored as a file `commonwords.txt`

- The phonetic lexicon created using Mlphon for the 100k common words is available as `./lexicons/commonwordslexicon.tsv`.  It is created by `lexiconcreator.py` script in tools directory. Possible multiple pronunciations are listed for words like എന്നാൽ, നിന്നാൽ etc. The script produces pronunciations for English abbreviations in Malayalam script, even though they are invalid as per Malayalam script grammar. eg: എൽഡിഎഫ്, ഐഎസ്ആർഒ.

- The `./lexicons/goldlexicon.tsv` has the  list of most frequent 1000 words from the above list and  gold standard transcription separated by tabs

- The `lexicons/lexicon_comparison.tsv` file has the list of most frequent 1000 words from the above list, gold standard transcription and the transcription provided by Mlphon toolkit separated by tabs.

- The pronunciations of words in `.lex` files from the Mlmorph project is generated and provided in the `./lexicons` directory namely; `nounlexicon.tsv`, `placeslexicon.tsv`, `verblexicon.tsv`, `nouns-sanskritlexicon.tsv`, `pronounlexicon.tsv`, `english-borrowedlexicon.tsv`, `person-nameslexicon.tsv`, `propernounslexicon.tsv` (Note that unanalysed words which have wrong script grammar were removed)

- All the above lexicons are made available in syllabified format too in `./lexicons` directory.

- `vocabASR` is a curated list of words which includes `commonwords.txt`, words from the acoustic training vocabulary for ASR and words from LM training corpus with at least 4 occurrences, used in the project https://gitlab.com/kavyamanohar/asr-malayalam 


